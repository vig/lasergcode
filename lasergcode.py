#!/usr/bin/env python
# coding=utf-8
"""
lasergcode: Inkscape extension to generate CNC laser gcode.
Copyright 2017 Greg Fawcett greg@vig.co.nz
Based on the awesome svg2gcode.py (https://github.com/vishpat/svg2gcode).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

'''
!!! CURRENTLY BROKEN !!!
I tried to upgrade this extension to Inkscape 1.2, which has a much changed
extension API. Most things are updated enough for the extension to work, but
there is a problem with transforms (hence all the debug lines) where the
object was scaled to a strange amount. I could not fix this in the time I
had.

Also it would be good to get preprocess() working, which converts everything
into paths. This would remove the need to do this manually before saving as
gcode.

Also would be nice to implement center zero point like
/usr/share/inkscape/extensions/hpgl_output.py does.

GCF 28 Oct 2024.
'''

# Update id tag in inx file when this changes.
lasergcode_current_version = "2,0"
debug = True

import sys
import os
import inkex
from inkex import Transform
from inkex.command import inkscape, write_svg, ProgramRunError
import cubicsuperpath
import cspsubdiv

#-----------------------------------------------------------------------
# Constants that should probably be settings
# $30: Set max spindle speed - 1000 = 5V
# $32: Enable laser mode (grbl1.1+)
# G90: Absolute distance mode
# G21: Millimeters
# M4: Dynamic laser power
preamble = ['$30=1000',
			'$32=1',
			'G90 G21 M4']
# M5: Laser off
# G0 X0 Y0: Return to start position.
postamble = ['M5 G0 X0 Y0']
flatness = 0.2

#-----------------------------------------------------------------------
def element_viewbox_transform(e, width_mm, height_mm):
	"""
	Get element viewbox transform.
	"""
	try:
		vb_str = e.get('viewBox')
		vx, vy, vwidth, vheight = (float(i) for i in vb_str.split())
		xratio = width_mm / vwidth
		yratio = height_mm / vheight
		return [[xratio, 0.0, -vx*xratio], [0.0, yratio, -vy*yratio]]
	except Exception:
		return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]

#-----------------------------------------------------------------------
def element_opacity(e):
	"""
	Get element e stroke opacity, or return 1.
	"""
	style_str = e.get('style')
	try:
		start_pos = style_str.index('stroke-opacity:')+16
		end_pos = style_str.find(';', start_pos)
		if end_pos == -1:
			return float(style_str[start_pos:].strip())
		else:
			return float(style_str[start_pos:end_pos].strip())
	except Exception:
		return 1.0

#-----------------------------------------------------------------------
def element_transform(e):
	"""
	Get element transform, or return the identity transform.
	"""
	transform_str=e.get('transform')
	if transform_str:
		return Transform(transform_str).matrix
	return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]

#-----------------------------------------------------------------------
def transform_str(t):
	"""
	Return a string describing a transform.
	"""
	return '[[%f, %f, %f],[%f, %f, %f]]'%(t[0][0], t[0][1], t[0][2], t[1][0], t[1][1], t[1][2])

#-----------------------------------------------------------------------
def write_lines(stream, lines):
	for line in lines:
		stream.write(line.encode('utf8'))
		stream.write(b'\n')


#=======================================================================
class LaserGcodeEffect(inkex.base.TempDirMixin, inkex.OutputExtension):

	#-----------------------------------------------------------------------
	def __init__(self):
		inkex.Effect.__init__(self)
		self.arg_parser.add_argument('--burn_rate', action = 'store',
			type = int, dest = 'burn_rate', default = '1000', help = 'Move rate during burns (mm/min)')
		self.arg_parser.add_argument('--repeat', action = 'store',
			type = int, dest = 'repeat', default = '1000', help = 'Repeat burn (times)')
		self.arg_parser.add_argument('--reverse_x', action = 'store',
			type = bool, dest = 'reverse_x', default = 'FALSE', help = 'Reverse X axis')
		self.arg_parser.add_argument('--reverse_y', action = 'store',
			type = bool, dest = 'reverse_y',	   default = 'FALSE', help = 'Reverse Y axis')
		self.gcode = []

	#-----------------------------------------------------------------------
	# Currently not working - self.svg seems un-changed (not changed to paths).
	# According to the docs, This should be defined by OutputExtension, but isn't.
	# https://inkscape.gitlab.io/extensions/documentation/_modules/inkex/extensions.html
	# def preprocess(self):
	# 	actions = [
	# 		"unlock-all",
	# 		# Flow roots contain rectangles inside them, so they need to be
	# 		# converted to paths separately from other shapes
	# 		"select-by-element:flowRoot",
	# 		"object-to-path",
	# 		"select-clear",
	# 	]
	# 	# Convert all non-paths to paths
	# 	elements = ["rect", "circle", "ellipse", "line", "polyline", "polygon", "text"]
	# 	actions += ["select-by-element:" + i for i in elements]
	# 	actions += ["object-to-path", "select-clear"]
	# 	# Unlink clones
	# 	actions += ["select-by-element:use", "object-unlink-clones"]
	# 	# Save and overwrite
	# 	actions += ["export-overwrite", "export-do"]
	# 	infile = os.path.join(self.tempdir, "input.svg")
	# 	write_svg(self.document, infile)
	# 	try:
	# 		inkscape(infile, actions=";".join(actions))
	# 	except ProgramRunError as err:
	# 		inkex.errormsg(_("An error occurred during document preparation"))
	# 		inkex.errormsg(err.stderr.decode("utf-8"))

	# 	with open(infile, "r") as stream:
	# 		self.document = inkex.load_svg(stream)
	# 	return self.document

	#-----------------------------------------------------------------------
	def recurse_group(self, group, parent_transform):
		"""
		Get gcode for any paths in group children, including recursing child groups.
		"""
		for child in group:
			try:
				etype = child.tag[child.tag.index('}')+1:] # Everything after the closing "}"
				assert etype in 'g|path'
			except Exception:
				inkex.utils.debug('Ignoring element ' + child.tag)
				continue

			# Recurse into child elements of group
			inkex.utils.debug('parent_transform:%s'%(parent_transform))
			inkex.utils.debug('element_transform:%s'%(Transform(element_transform(child))))
			child_transform = Transform(parent_transform) @ Transform(element_transform(child))
			inkex.utils.debug('child_transform:%s'%(child_transform))

			if etype == 'g':
				self.recurse_group(child, child_transform)

			# Add gcode for the path to self.gcode
			elif etype == 'path':
				# Set laser power. We set $30=1000 in preamble.
				new_power = int(element_opacity(child) * 1000)
				if new_power != self.power:
					self.power = new_power
					self.gcode.append('S%d'%new_power)

				# Convert SVG path data into sets of subpaths of cubic spline points.
				super_path = cubicsuperpath.parsePath(child.get('d'))
				# Transform cubic spline points.
				for sub_path in super_path:
					for point_set in sub_path:
						for point in point_set:
							inkex.utils.debug('point1:%s'%(point))
							# point[0] = child_transform[0][0]*point[0] + child_transform[0][1]*point[1] + child_transform[0][2]
							# point[1] = child_transform[1][0]*point[0] + child_transform[1][1]*point[1] + child_transform[1][2]
							point = child_transform.apply_to_point(point)
							inkex.utils.debug('point2:%s'%(point))
					# Subdivide curves.
					cspsubdiv.subdiv(sub_path, flatness)
					command = 'G0'
					for point_set in sub_path:
						x, y = point_set[1][0], point_set[1][1]
						if (0 <= x <= self.width) and (0 <= y <= self.height):
							self.gcode.append('%s X%0.1f Y%0.1f'%(command, x, y))
							if command == 'G0':
								command = 'G1'
						else:
							inkex.utils.debug('Point %0.2f, %0.2f is outside drawing - ignored.'%(x, y))


	#-----------------------------------------------------------------------
	def save(self, stream):

		preamble.append('F%d'%self.options.burn_rate)

		# self.preprocess()
		root = self.svg.root

		# Get doc width and height in mm
		self.width = self.svg.uutounit(self.svg.unittouu(root.get('width')), 'mm')
		self.height = self.svg.uutounit(self.svg.unittouu(root.get('height')), 'mm')
		if debug: inkex.utils.debug('Document width:%fmm height:%fmm'%(self.width, self.height))

		# Get transform from user units to mm from viewBox
		svg_transform = element_viewbox_transform(root, self.width, self.height)
		if debug: inkex.utils.debug('viewBox transform: ' + transform_str(svg_transform))
		if self.options.reverse_x:
			svg_transform[0][0] =- svg_transform[0][0]
			svg_transform[0][2] =- svg_transform[0][2]+self.width
			if debug: inkex.utils.debug('Reversed-X transform: ' + transform_str(svg_transform))

		# SVG Y-axis is already reversed, so NOT.
		if not self.options.reverse_y:
			svg_transform[1][1] =- svg_transform[1][1]
			svg_transform[1][2] =- svg_transform[1][2]+self.height
			if debug: inkex.utils.debug('Reversed-Y transform: ' + transform_str(svg_transform))

		self.power = 0
		self.recurse_group(root, svg_transform)

		write_lines(stream, preamble)
		while self.options.repeat:
			write_lines(stream, self.gcode)
			self.options.repeat -= 1
		write_lines(stream, postamble)


#-----------------------------------------------------------------------
if __name__ == '__main__':
	LaserGcodeEffect().run()

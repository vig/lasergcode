# Inkscape lasergcode Extension #

* Version 1.0

This extension creates Gcode suitable for laser engravers (like the Eleksmaker range) from Inkscape documents.

It is much, _much_, MUCH easier to use than GcodeTools, and
includes feed rate, repeat  and X- and Y-axis reversal options.

Because Inkscape supports MacOS and linux as well as Windows, it provides a good solution for driving these
laser engravers from all operating systems.

### Liability ###

By installing and using the lasergcode extension, you accept all responsibility
for any damage or harm that may occur, and exclude the author(s) of
this software from liability for any damages whatsoever.

### Requirements ###

* Your laser controller must be running Grbl 1.1 or later. See [these instructions](https://github.com/gnea/grbl/wiki/Flashing-Grbl-to-an-Arduino).
* You need [Inkscape](https://inkscape.org/).

### Installing the lasergcode Extension ###

1. Copy `lasergcode.py` and `lasergcode.inx` into the extensions directory:
	* For Windows `C:\Program Files\Inkscape\share\extensions\`
	* For Mac `~/.config/inkscape/extensions/`
	* For Linux `/usr/share/inkscape/extensions`
2. (Re)start Inkscape.
3. Choose _File/Save a Copy..._ and confirm the file type dropdown contains _Laser gcode file (*.gcode)_.

### Using lasergcode ###

It is important to understand three things:

1. **Only path objects are rendered to gcode.** Everything else (text, boxes,
shapes etc.) is ignored. Luckily Inkscape has a very easy way to convert
these other objects to paths (_Path/Object to Path_).
2. **The laser intensity is controlled by the alpha channel of the stroke colour.**
The `intensity_chart.svg` and `intensity_chart.gcode` files demonstrate this,
and the gcode file is useful as an initial test to choose what intensity
you need for a new material.
3. **Path segments outside the document dimensions are ignored.** So setting
the Inkscape document width and height to be equal or less than the movement
limits of your laser engraver means you will never drive the laser head into
the frame, which could damage the servo motors and driver circuits and burn
one point for a long time.

OK, let's make a gcode file:

* Create a new Inkscape document.
* Set the document properties _File/Document Properties_:
	* Custom size width: How far your laser can travel sideways.
	* Custom size height: How far your laser printer can travel up and down.
* Draw something.
* Convert all objects in your document to paths, by selecting them and choosing _Path/Object to Path_.
* Set intensity by:
	* Selecting object(s).
	* Open the _Fill and Stroke_ control (Shift+Ctrl+F).
	* Choose the _Stroke paint_ tab.
	* Set the alpha (A) channel between 0 (no power) and 255 (full power).
* Choose _File/Save a Copy..._, select _Laser gcode file (*.gcode)_ and click _Save_.
* Set the burn speed and repeat number, and click _OK_.
* PUT ON YOUR PROTECTIVE GLASSES!
* Use something like [Pronterface](https://www.pronterface.com/) or [Universal Gcode Sender](https://github.com/winder/Universal-G-Code-Sender)
to send the gcode file to your laser engraver.

### Problems? ###

If lasergcode does not do what you expect, first check the three points at
the start of the _Using lasergcode_ section.

If this doesn't help, go to [the lasergcode issue tracker](https://bitbucket.org/vig/lasergcode/issues)
and create an issue. You will get much better help if you include:

* A detailed explanation of the problem.
* A detailed explanation of how to re-create the problem.
* The SVG file.

If you know any Python, you can hack the `lasergcode.py` file to try and
fix the problem. Just restart Inkscape each time you make a change.


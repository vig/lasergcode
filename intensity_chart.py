#!/usr/bin/env python
# coding=utf-8
"""
intensity_chart.py: Generate the gcode for an intensity chart.
Copyright 2018 Greg Fawcett greg@vig.co.nz
"""

# Return
def write_block(block_xys, min_x, min_y, dest):
	if not block_xys: # Ignore empty blocks
		return None, None
	xs=set()
	ys=set()
	last_x=last_y=None
	diagonal=False
	for x,y in block_xys:
		xs.add(x)
		ys.add(y)
		if last_x is not None and x!=last_x and y!=last_y:
			diagonal=True
		last_x=x
		last_y=y
	if len(xs)==len(ys)==2 and not diagonal: # A square
		return sorted(xs), sorted(ys)
	else: # Text
		g0=True
		for x,y in block_xys:
			dest.write('%s X%0.1f Y%0.1f\n'%('G0' if g0 else 'G1', x, y))
			g0=False
		return  None, None

squares=[]
text_count=0
block_xys=[]
min_x=min_y=9999
postamble=''

with open('intensity_speed_chart.gcode', 'w') as dest:
	with open('intensity_chart.gcode', 'r') as source:
		for line in source:
			# Ignore intensity and feed
			if line[0]=='S'or line[0]=='F':
				continue
			if line.startswith('M5'):
				postamble=line
				continue
			# Copy non-G lines as is
			elif line[0]!='G' or line[1] not in '01':
				dest.write(line)
				continue
			if line[1]=='0': # G0 - A new block
				if not block_xys:
					dest.write('S500\nF500\n') # Add intensity and feed for text
				xs,ys=write_block(block_xys, min_x, min_y, dest)
				if xs and ys:
					squares.append((xs,ys))
				elif block_xys:
					text_count+=1
				# Reset block_xys and mins
				block_xys=[]
				min_x=min_y=9999
			g,x,y=line.split()
			x=float(x[1:])
			y=float(y[1:])
			if x<min_x:
				min_x=x
			if y<min_y:
				min_y=y
			block_xys.append((x,y))

		# Write last block
		xs,ys=write_block(block_xys, min_x, min_y, dest)
		if xs and ys:
			squares.append((xs,ys))
		elif block_xys:
			text_count+=1

		# We assume squares are in the right order
		i=0
		for xs,ys in squares:
			feed, intensity=divmod(i, 5)
			if not intensity:
				dest.write('F%d\n'%(200*(feed+1)))
			dest.write('S%d\n'%((50*(intensity+1))/0.255))
			dest.write('G0 X%0.1f Y%0.1f\n'%(xs[0], ys[1]))
			dest.write('G1 X%0.1f Y%0.1f\n'%(xs[1], ys[1]))
			dest.write('G1 X%0.1f Y%0.1f\n'%(xs[1], ys[0]))
			dest.write('G1 X%0.1f Y%0.1f\n'%(xs[0], ys[0]))
			dest.write('G1 X%0.1f Y%0.1f\n'%(xs[0], ys[1]))
			i+=1

		dest.write(postamble)

print '%d squares and %d text'%(len(squares), text_count)
